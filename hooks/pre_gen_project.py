#!/usr/bin/env python3
{% raw %}
# vim: set filetype=python tw=100 cc=+1:

import logging
import argparse
import sys
import types
import os
import os.path
import pathlib
import json
import subprocess
import urllib.request
import jinja2
import jinja2.sandbox
import datetime

import coloredlogs

# https://cookiecutter.readthedocs.io/en/latest/advanced/hooks.html

logger = logging.getLogger(__name__ if __name__ != "__main__" else "hooks.pre_gen_project")

script_dirname = os.path.dirname(__file__) # pylint: disable=invalid-name
script_dirnameabs = os.path.abspath(script_dirname) # pylint: disable=invalid-name
script_basename = os.path.basename(__file__) # pylint: disable=invalid-name

script_path = pathlib.Path(__file__) # pylint: disable=invalid-name

cookie_path = script_path.parent
cookie_dirname = os.path.join(script_dirname, "..")
cookie_dirnameabs = os.path.abspath(cookie_dirname)


{% endraw %}
cookiecutter_json = """{{ cookiecutter | tojson('  ') }}"""
{% raw %}
cookiecutter = json.loads(cookiecutter_json);

class Main:
    def __init__(self):
        logger.info("entry ...")
        pass

    def exec(self):
        logger.info("entry: ...")
        logger.info("os.getcwd() = %s", os.getcwd())
        logger.info("__file__ = %s", __file__)
        logger.info("script_dirname = %s", script_dirname)
        logger.info("cookie_dirname = %s", cookie_dirname)
        logger.info("cookie_dirnameabs = %s", cookie_dirnameabs)
        logger.info("cookiecutter_json = %s", cookiecutter_json)

        for key, value in os.environ.items():
            logger.info("os.environ[%s] -> %s", key, value);

        env = jinja2.sandbox.SandboxedEnvironment(keep_trailing_newline=True)

        license_template_url = cookiecutter.setdefault("license_template_url",
            ("https://gitlab.com/aucampia/license-info/raw/master"
                "/licenses/{license_code}/template-width_CANON.cc.j2").format(**cookiecutter))
        if cookiecutter["license_code"] != "UNLICENSED":
            cookiecutter.setdefault("license_preamble",
                "{license_code} License\n\n".format(**cookiecutter))
            rights = ""
        else:
            cookiecutter.setdefault("license_preamble", "")
            rights = " - All rights reserved."


        cookiecutter.setdefault("license_copyright_header",
            "Copyright (c) {current_year} {full_name}{rights}".format(**cookiecutter,
                current_year=datetime.datetime.now().strftime("%Y"), rights=rights))

        logger.info("license_template_url = %s", license_template_url)
        request = urllib.request.Request(license_template_url)
        with urllib.request.urlopen(request) as url:
            license_template_string = url.read().decode("utf-8")

        logger.info("license_template_string = %s", license_template_string)
        license_template = env.from_string(license_template_string);

        with open("LICENSE.txt", "wb+") as file_handle:
            file_handle.write(license_template.render(cookiecutter=cookiecutter).encode("utf-8"))


def exec_main():
    """
    logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%dT%H:%M:%S", stream=sys.stderr,
        format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))
    """

    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stderr)
    handler.setFormatter(coloredlogs.ColoredFormatter(datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=("%(asctime)s %(process)d %(thread)x %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")
        ))
    root_logger.addHandler(handler)
    main = Main()
    main.exec()

if __name__ == "__main__":
    exec_main()

{% endraw %}
