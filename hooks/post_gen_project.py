#!/usr/bin/env python3
{% raw %}
# vim: set filetype=python tw=100 cc=+1:

import logging
import argparse
import sys
import types
import os
import os.path
import pathlib
import shutil
import json
import subprocess

import coloredlogs

import distutils.dir_util

# https://cookiecutter.readthedocs.io/en/latest/advanced/hooks.html

logger = logging.getLogger(__name__ if __name__ != "__main__" else "hooks.post_gen_project")

script_dirname = os.path.dirname(__file__) # pylint: disable=invalid-name
script_dirnameabs = os.path.abspath(script_dirname) # pylint: disable=invalid-name
script_basename = os.path.basename(__file__) # pylint: disable=invalid-name

{% endraw %}
cookiecutter_json = """{{ cookiecutter | tojson('  ') }}"""
{% raw %}
cookiecutter = json.loads(cookiecutter_json)

namespace_init='''#!/usr/bin/env python3
# vim: set filetype=python tw=100 cc=+1:
# https://setuptools.readthedocs.io/en/latest/pkg_resources.html#id5
# https://www.python.org/dev/peps/pep-0420/#namespace-packages-today
"""
This is a namespace package in line with PEP-0420.
"""

__path__ = __import__('pkgutil').extend_path(__path__, __name__)
'''

class Main:
    def __init__(self):
        logger.info("entry ...")
        pass

    def exec(self):
        logger.info("entry: os.getcwd() = %s, script_dirname = %s", os.getcwd(), script_dirname)
        logger.info("cookiecutter_json = %s", cookiecutter_json)

        cwd_path = pathlib.Path.cwd()

        tdir_package_path = cwd_path.joinpath("src", "tdir-package")
        namespace_parts = cookiecutter["python_package_fqname"].split('.')
        logger.info("namespace_parts = %s", namespace_parts)
        namespace_path = cwd_path.joinpath("src" ,*namespace_parts)
        logger.info("will make namespace_path.parent %s", namespace_path.parent)
        namespace_path.parent.mkdir(parents=True, exist_ok=True)
        logger.info("will make namespace_path %s", namespace_path)
        namespace_path.mkdir(parents=True, exist_ok=True)
        logger.info("will copytree tdir_package_path %s to namespace_path %s",
            tdir_package_path, namespace_path)
        distutils.dir_util.copy_tree(str(tdir_package_path), str(namespace_path),
            preserve_mode=0, preserve_times=0,
            preserve_symlinks=1, update=1, verbose=1)
        logger.info("will rmtree tdir_package_path %s",
            tdir_package_path)
        shutil.rmtree(tdir_package_path)
        #def my_copy(*args, **kwargs):
        #    logger.info("copy2: %s -> %s", args[0], args[1])
        #    shutil.copy2(*args, **kwargs)
        #shutil.copytree(str(tdir_package_path), str(namespace_path),
        #    copy_function=my_copy, symlinks=True)
        #logger.info("will move tdir_package_path %s to namespace_path %s",
        #    tdir_package_path, namespace_path)
        #shutil.move(str(tdir_package_path), str(namespace_path))

        cookiecutter_input_path = cwd_path.joinpath("cookiecutter-input.yaml")
        if not cookiecutter_input_path.exists():
            try:
                import yaml
                logger.info("Writing cookiecutter input to %s", cookiecutter_input_path)
                with open(cwd_path.joinpath("cookiecutter-input.yaml"), "w") as file_object:
                    data = {"default_context": cookiecutter.copy()}
                    del data["default_context"]["_template"]
                    #json.dump(data, file_object, indent=2, sort_keys=True)
                    yaml.safe_dump(data, file_object)
                    #file_object.write('\n')
            except ImportError:
                logger.warning("no yaml, %s will not be written - install pyyaml to fix",
                    cookiecutter_input_path)
        else:
            logger.info("Not writing %s as it already exists", cookiecutter_input_path)

        for index, part in enumerate(namespace_parts[0:-1]):
            namespace_path = os.path.join("src", *namespace_parts[0:index + 1], "__init__.py")
            logger.info("namespace_path = %s", namespace_path)
            with open(namespace_path, "wb+") as file_object:
                file_object.write(namespace_init.encode("utf-8"))

        result = subprocess.run(["versioneer", "install"])
        result = subprocess.run(["make", "versioneer"])

def exec_main():
    """
    logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%dT%H:%M:%S", stream=sys.stderr,
        format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))
    """

    root_logger = logging.getLogger("")
    root_logger.propagate = True
    root_logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stderr)
    handler.setFormatter(coloredlogs.ColoredFormatter(datefmt="%Y-%m-%dT%H:%M:%S",
        fmt=("%(asctime)s %(process)d %(thread)x %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")
        ))
    root_logger.addHandler(handler)
    main = Main()
    main.exec()

if __name__ == "__main__":
    exec_main()

{% endraw %}
