# ...

[![maintenance: sprodadic](https://img.shields.io/badge/maintenance-sporadic-red.svg)](.)

Example content for `cookiecutter-input.yaml`
```yaml
default_context:
  full_name: Iwan Aucamp
  project_name: aucampia-template
  python_package_fqname: iwana.template
  project_url: https://gitlab.com/aucampia/templates/python
  project_dirname: aucampia-template
  project_short_description: aucampia-template
  cli_name: iwana.template.cli.py
  license_code: CC0-1.0
```

```bash
## using cookiecutter-input.yaml
cookiecutter -v --no-input --overwrite-if-exists --output-dir ../ \
    --config-file cookiecutter-input.yaml \
    https://gitlab.com/aucampia/cookiecutters/python.git

## output dir is ../ because cookiecutter operates on a subdirectory by default
make ccut-check
make ccut-check-diff
```
