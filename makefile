SHELL=bash
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

cut-%: data/vars-%.yaml
	mkdir -p build/out-cut/
	rm -vrf build/out-cut/$(*)
	# cookiecutter ./ --no-input --config-file $(<) -o build/out-cut/$(*)
	cookiecutter -v ./ --overwrite-if-exists --no-input --config-file $(<) -o build/out-cut/

cuti-%: data/vars-%.yaml
	mkdir -p build/out-cuti/
	rm -vrf build/out-cuti/$(*)
	# cookiecutter ./ --no-input --config-file $(<) -o build/out-cut/$(*)
	grep . $(<)
	cookiecutter -v ./ --overwrite-if-exists -o build/out-cuti/

clean:
	rm -vrf build/out-cut/
