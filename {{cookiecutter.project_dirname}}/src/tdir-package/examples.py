#!/usr/bin/env python3
# vim: set tw=100 cc=+1:
# pylint: disable=missing-docstring
"""
This module contains various examples.
"""
GLOBAL_VARIABLE="GLOBAL_VARIABLE:VALUE"

def some_function(some_arg):
    """
    This is an example function:

    :param str some_arg: This is just some argument which is concatenated with a hard coded string.
    :returns: the value of some_arg concatenated with some hard coded string
    :rtype: str
    """
    return "some_function:some_string:" + some_arg

class SomeClass:
    # pylint: disable=too-few-public-methods
    """
    Some class
    """
    def __init__(self):
        """
        Constructor for the class ...
        """
        self.something = "somevalue"

    def method(self):
        return self.something
