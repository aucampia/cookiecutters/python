#!/usr/bin/env python3
# vim: set tw=100 cc=+1:
# pylint: disable=missing-docstring

# https://docs.python.org/2/distutils/sourcedist.html
# https://docs.python.org/2/distutils/configfile.html
# https://docs.python.org/2/distutils/setupscript.html#additional-meta-data

# https://docs.python.org/3/distutils/sourcedist.html
# https://docs.python.org/3/distutils/introduction.html#distutils-simple-example
# https://docs.python.org/3/distutils/setupscript.html#additional-meta-data

# https://setuptools.readthedocs.io/en/latest/
# https://setuptools.readthedocs.io/en/latest/setuptools.html#new-and-changed-setup-keywords

import os
import setuptools
import versioneer

SCRIPT_DIRNAME = os.path.dirname(__file__)
SCRIPT_DIRNAMEA = os.path.abspath(SCRIPT_DIRNAME)
SCRIPT_BASENAME = os.path.basename(__file__)

DESCRIPTION_BASENAME = "DESCRIPTION.md"
DESCRIPTION_PATH = os.path.join(SCRIPT_DIRNAME, DESCRIPTION_BASENAME)
if os.path.exists(DESCRIPTION_PATH):
    with open(DESCRIPTION_PATH, encoding="utf-8") as file_object:
        DESCRIPTION_CONTENT = file_object.read()
else:
    DESCRIPTION_CONTENT = """{{ cookiecutter.project_short_description
        | default(cookiecutter.project_name) }}"""

setuptools.setup(
    name="{{ cookiecutter.python_package_fqname }}",
    url="{{ cookiecutter.project_url }}",
    description="{{ cookiecutter.project_short_description }}",
    long_description=DESCRIPTION_CONTENT,
    long_description_content_type="text/markdown",
    license="{{ cookiecutter.license_code }}",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    package_dir={"":"src"},
    #packages=setuptools.find_namespace_packages(where="src", exclude=["contrib", "docs", "tests"]),
    packages=setuptools.find_packages(where="src", exclude=["contrib", "docs", "tests"]),
    py_modules=[],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "{{ cookiecutter.cli_name }}={{ cookiecutter.python_package_fqname }}.cli:main",
        ],
    },
    install_requires=[
    ],
    setup_requires=[
        "pytest-runner",
        "pytest-pylint"
    ],
    tests_require=[
        "pytest",
        "pylint"
    ],
    extras_require={
        "dev": ["check-manifest"],
        "test": ["coverage", "pytest", "pytest-pylint", "pylint"],
    },
    zip_safe=False,
    # https://pypi.org/classifiers/
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
    ],
    keywords="",
    python_requires=">=3, <4",
)
