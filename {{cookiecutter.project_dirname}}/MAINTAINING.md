# {{ cookiecutter.project_name }} / Maintainer Guide

```bash
make einstall test
git tag --message="tag version" 0.1.0
git push --tags
make wheel-upload
```
