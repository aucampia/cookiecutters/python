# ...

## TODO

* [ ] cleanup config
* [ ] Fix data ... https://docs.python.org/2/distutils/setupscript.html
* [x] ...

## Dev

### Cycle

```bash
diff -u -r -x .git -x __pycache__ -x .eggs link-project/ ~/d/gitlab.com/aucampia/templates/python/ | view -
diff -u -r -x .git -x __pycache__ -x .eggs link-package/ ~/d/gitlab.com/aucampia/templates/python/src/iwana/template/ | view -

make clean cut-aucampia
diff -u -r -x .git -x __pycache__ -x .eggs build/out-cut/aucampia-template/ ~/d/gitlab.com/aucampia/templates/python/ | view -
make clean cuti-aucampia
diff -u -r -x .git -x __pycache__ -x .eggs build/out-cuti/aucampia-template/ ~/d/gitlab.com/aucampia/templates/python/ | view -
make -C build/out-cut/aucampia-template/ lint
```

### Trash

```bash
pip3 install --user --upgrade cookiecutter
mkdir -p local/out-cut/
cookiecutter ./ -o local/out-cut/

make cut-uiyaejei
make cut-test-000

make cut-aucampia
diff -u -r -x __pycache__ -x .git -x .eggs ~/d/gitlab.com/aucampia/templates/python/ ~/d/gitlab.com/aucampia/cookiecutters/python/local/out-cut/aucampia-template/ | egrep '^(Only|diff)'
```

 - https://cookiecutter.readthedocs.io/en/latest/tutorial1.html#cookiecutter-json
 - https://cookiecutter.readthedocs.io/en/latest/readme.html#a-pantry-full-of-cookiecutters
 - https://github.com/veit/cookiecutter-namespace-template
 - https://github.com/kragniz/cookiecutter-pypackage-minimal
 - https://github.com/audreyr/cookiecutter-pypackage
 - https://github.com/wdm0006/cookiecutter-pipproject

